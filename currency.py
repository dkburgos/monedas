import os
import datetime                  #puedes importaar un modulo en cada linea...
import requests, json, openpyxl  # ...o muchos
from openpyxl import Workbook

#VARIABLES
base_currency = "EUR" #get the exchange data from the URL for COP
excel_file = 'monedaeuro.xlsx'

#prepare the excel sheet
if not os.path.isfile(excel_file): #if the file doesn't exist yet, create it
    wb = Workbook()
    ws = wb.active # grab the active worksheet
    ws['A1']  = "Wechselkurse {}-ander Währungen".format(base_currency)
    ws['A2']  = "Datum"
else:
    wb = openpyxl.load_workbook(filename=excel_file)
    ws = wb.active # grab the active worksheet

#get the data from the internet
response = requests.get("https://api.exchangerate-api.com/v4/latest/" + base_currency)
json_data = json.loads(response.text)
print(response.text) # PRINT THE DATA TO THE COMMAND LINE

#write the currency names to the top
for index, heading_row in enumerate(json_data["rates"].keys()):
    ws.cell(row=2, column= 2 + index, value=heading_row)

#write the date + the new currency data to the last row
new_data = []
new_data += [str(datetime.datetime.now())] # add two arrays => result is a bigger array
                                           # gets the current time and writes the str to the beginning of the row
for index, heading_row in enumerate(json_data["rates"].values()):
    new_data.append(heading_row)
ws.append(new_data) # Rows can also be appended
                    # adds the new data as the last row
wb.save(excel_file)
